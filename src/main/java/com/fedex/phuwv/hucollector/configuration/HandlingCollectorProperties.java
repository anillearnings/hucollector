
package com.fedex.phuwv.hucollector.configuration;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@ConfigurationProperties(prefix = "phuwv.view")
@Slf4j
@Getter
@Setter
@NoArgsConstructor
public class HandlingCollectorProperties {

	private int maxDataImgColumnLength;
	private int maxDataImgColumn;
	private int maxRetentionDays;
	private int nbrDaysAfterHandlingUnitDtToPurge;
	private boolean enableCalcPurgeDate;

	@PostConstruct
	public void initialize() {
		log.info(" maxDataImgColumnLength Property initialized" +maxDataImgColumnLength);
    	log.info(" maxDataImgColumn Property initialized" +maxDataImgColumn);
    	log.info(" maxRetentionDays Property initialized" +maxRetentionDays);
    	log.info(" nbrDaysAfterHandlingUnitDtToPurge Property initialized" +nbrDaysAfterHandlingUnitDtToPurge);
    	log.info(" enableCalcPurgeDate Property initialized" +enableCalcPurgeDate);
		log.info("Properties initialized");
	}

}
