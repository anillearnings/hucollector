package com.fedex.phuwv.hucollector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitId;

@Repository
public interface HandlingUnitRepository extends JpaRepository<HandlingUnitEntity, HandlingUnitId> {

	public List<HandlingUnitEntity> findByHandlingUnitId_handlingUnitUUID(String handlingUnitUUID);

	public List<HandlingUnitEntity> findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(
            String handlingUnitUUID);

}
