package com.fedex.phuwv.hucollector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefEntity;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefId;


@Repository
public interface HandlingUnitXRefRepository extends JpaRepository<HandlingUnitXRefEntity, HandlingUnitXRefId> {
    
    public List<HandlingUnitXRefEntity> findByHandlingUnitRefId_trackingNbr(String trackingNbr);

	public List<HandlingUnitXRefEntity> findByHandlingUnitRefId_handlingUnitUUID(String uuid);

}
