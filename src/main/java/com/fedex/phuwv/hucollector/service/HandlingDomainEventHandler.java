package com.fedex.phuwv.hucollector.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.exception.ApplicationException;
import com.fedex.phuwv.common.exception.ResourceException;
import com.fedex.phuwv.common.exception.RetryableException;
import com.fedex.phuwv.common.exception.classifier.ExceptionClassifier;
import com.fedex.phuwv.common.jms.JMSPublisher;
import com.fedex.phuwv.common.jms.MessageHandler;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.common.types.StatusTypes;
import com.fedex.phuwv.common.validator.JsonValidator;
import com.fedex.phuwv.hucollector.configuration.HandlingCollectorProperties;
import com.fedex.phuwv.hucollector.repository.HandlingUnitRepository;
import com.fedex.phuwv.hucollector.repository.HandlingUnitXRefRepository;
import com.fedex.phuwv.util.BytesUtil;
import com.fedex.phuwv.util.CompressionUtil;
import com.fedex.phuwv.util.JsonUtil;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitId;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefEntity;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefId;
import com.fedex.sefs.core.handlingunit.v1.api.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.RetryContext;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.util.CollectionUtils;

import javax.jms.JMSException;
import javax.jms.Message;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static com.fedex.phuwv.hucollector.util.HandlingUnitCollectorConstant.*;

@Service("HandlingDomainEventHandler")
@Slf4j
public class HandlingDomainEventHandler implements MessageHandler<HandlingUnitDomainEvent> {
    private ExceptionClassifier<RetryableException> exceptionClassifier;
    private ObjectMapper objectMapper;
    private HandlingUnitRepository repository;
    private HandlingUnitXRefRepository huXRepository;
    private RetryTemplate retryTemplate;
    private JsonValidator jsonValidator;
    private HandlingCollectorProperties handlingCollectorProperties;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private JMSPublisher domainEventPublisher;

    @Autowired
    public HandlingDomainEventHandler(ExceptionClassifier<RetryableException> exceptionClassifier,
                                      @Qualifier("defaultObjectMapper") ObjectMapper objectMapper, HandlingUnitRepository repository,
                                      HandlingUnitXRefRepository huXRepository, @Qualifier("jmsRetryTemplate") RetryTemplate retryTemplate,
                                      @Qualifier("jsonValidator") JsonValidator jsonValidator,
                                      HandlingCollectorProperties handlingCollectorProperties) {

        this.exceptionClassifier = exceptionClassifier;
        this.objectMapper = objectMapper;
        this.repository = repository;
        this.huXRepository = huXRepository;
        this.retryTemplate = retryTemplate;
        this.jsonValidator = jsonValidator;
        this.handlingCollectorProperties = handlingCollectorProperties;
    }

    @Override
    public void handle(GenericMessage<HandlingUnitDomainEvent> genericMessage) throws Exception {
        retryTemplate.execute(retryContext -> {
            TransactionStatus transactionStatus = getTrxStatus();
            try {
                HandlingUnitDomainEvent domainEvent = genericMessage.getPayload();

                log.info("HandlingUnitDomainEvent_JMS UUID: {} ",
                        String.valueOf(domainEvent.getHandlingUnit().getSystemIdentificationProfile().getUuid()));

                if (log.isDebugEnabled()) {
                    String json = JsonUtil.asCompactJson(objectMapper, domainEvent);
                    log.info("HandlingUnitDomainEvent_JMS_payload: {}", json);
                }
                jsonValidator.validate(domainEvent);
                HandlingUnit handlingUnit = domainEvent.getHandlingUnit();
                String handlingUUID = null;
                if (Objects.nonNull(handlingUnit)) {
                    SystemIdentificationProfile idProfile = handlingUnit.getSystemIdentificationProfile();
                    if (idProfile != null) {
                        handlingUUID = String.valueOf(idProfile.getUuid());
                    }
                }
                saveHandlingUnitEntity(domainEvent, handlingUUID);
                
                List<HandlingUnitXRefEntity> xRef = huXRepository.findByHandlingUnitRefId_handlingUnitUUID(handlingUUID);
                if ((xRef.isEmpty())) {
                    saveHuxdata(domainEvent, handlingUUID);
                }
                try {
                    transactionManager.commit(transactionStatus);
                    log.info("Transaction on commit is done to the DB");
                } catch (Exception e) {
                    log.info("Exception while saving in DB: "+e.getMessage());
                }
                publishDomainEvent(domainEvent);
                Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
                return null;
            } catch (Exception e) {
                if (!transactionStatus.isCompleted()) {
                    transactionManager.rollback(transactionStatus);
                    log.info("Transaction has been rollbacked to the DB",e.getMessage());
                   
                }
                throw e;
            }
        }, retryContext -> {
            setRetryContext(retryContext);
            return null;
        });
    }

    private void saveHandlingUnitEntity(HandlingUnitDomainEvent domainEvent, String handlingUUID) throws IOException {
        if (Objects.nonNull(domainEvent.getDomainEventHeader())) {
            String eventUUID = String.valueOf(domainEvent.getDomainEventHeader().getEventUUID());
            String eventType = String.valueOf(domainEvent.getDomainEventHeader().getEventType());
            ZonedDateTime eventCreateDateTime = domainEvent.getDomainEventHeader().getCreateEventDateTime();
            Audit.getInstance().put(AuditKey.TX_ID, eventUUID);
            String input = eventUUID + ":" + eventType + ":" + handlingUUID;
            Audit.getInstance().put(AuditKey.IN, input);
            String shipmentUUID = getShipmentId(domainEvent);
            HandlingUnitEntity entity = new HandlingUnitEntity();
            HandlingUnitId handlingUnitId = new HandlingUnitId(handlingUUID, eventUUID, Instant.now());
            entity.setHandlingUnitId(handlingUnitId);
            entity.setEventType(eventType);
            entity.setShipmentUUID(shipmentUUID);
            entity.setEventCreateTimestamp(eventCreateDateTime);
            entity.setRowPurgeDt(getDefaultPurgeDate());
            String json = JsonUtil.asCompactJson(objectMapper, domainEvent);
            convertJsonToRaw(json, entity);
            repository.save(entity);
            log.info("Saved handling unit entity to the DB for handlingUUID "+handlingUUID);
        }
    }

    private void setRetryContext(RetryContext retryContext) {
        RetryableException retryableException = null;
        try {
            retryableException = exceptionClassifier.classify(retryContext.getLastThrowable());
        } catch (Throwable e) {
            throw new ApplicationException("Failed to recover: " + ExceptionUtils.getFullStackTrace(e), e);
        }
        if (retryableException != null) {
            String json = null;
            throw new ResourceException(
                    "Failed to process domain event: " + json + " due to " + retryableException.getMessage(),
                    retryableException);
        }
    }

    private void saveHuxdata(HandlingUnitDomainEvent domainEvent, String handlingUUID) {
        HandlingUnit huId = domainEvent.getHandlingUnit();
        if (Objects.nonNull(huId)) {
            HandlingUnitIdentificationProfile huidp = huId.getIdentificationProfile();
            if (Objects.nonNull(huidp)) {
                List<com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitId> huids = huidp.getHandlingUnitIds();
                if (!CollectionUtils.isEmpty(huids)) {
                    for (com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitId hu : huids) {
                        HandlingUnitXRefId key = new HandlingUnitXRefId();
                        HandlingUnitXRefEntity crossRefEntity = new HandlingUnitXRefEntity();
                        String trackingNbr = hu.getId();
                        String trackNbrType = hu.getIdType();
                        key.setHandlingUnitUUID(handlingUUID);
                        key.setTrackingNbrType(trackNbrType);
                        key.setTrackingNbr(trackingNbr);
                        crossRefEntity.setHandlingUnitRefId(key);
                        crossRefEntity.setRowPurgeData(getDefaultPurgeDate());
                        huXRepository.save(crossRefEntity);
                        log.info("Saved handling unit Xref entity to the DB for handlingUUID "+handlingUUID);
                    }
                }
            }
        }
    }

    private Date getDefaultPurgeDate() {
        Instant now = Instant.now();
        Instant purgeInstant = now.plus(handlingCollectorProperties.getMaxRetentionDays(), ChronoUnit.DAYS);
        Date date = Date.from(purgeInstant);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    private void convertJsonToRaw(String compactJson, HandlingUnitEntity entity) throws IOException {
        int maxColumnLength = handlingCollectorProperties.getMaxDataImgColumnLength();
        int totalColumnLength = handlingCollectorProperties.getMaxDataImgColumn() * maxColumnLength;
        byte[] byteBuff = CompressionUtil.gzip(compactJson);
        List<byte[]> dataImages = BytesUtil.convertJsonByteArray(byteBuff, maxColumnLength, totalColumnLength);
        int dataImageToSet = dataImages.size();
        entity.setDataImg1(dataImages.get(0));
        if (dataImageToSet > 1) {
            entity.setDataImg2(dataImages.get(1));
        }
        if (dataImageToSet > 2 && log.isErrorEnabled()) {
            log.error("Data Image Size={}", dataImageToSet);
            log.error("COMPACT_JSON_SIZE={}, COMPRESSED_JSON_SIZE={}", compactJson.length(), byteBuff.length);
        }
    }

    protected Date getCalculatePurgeDateByHandling(HandlingUnitDomainEvent domainEvent) throws ParseException {
        HandlingUnit handlingUnit = domainEvent.getHandlingUnit();
        if (null != handlingUnit) {
            HandlingUnitIdentificationProfile handlingIdProfile = handlingUnit.getIdentificationProfile();
            if (null != handlingIdProfile) {
                ZonedDateTime zonedDateTime = handlingIdProfile.getIntendedShipDate();
                if (null != zonedDateTime) {
                    ZonedDateTime adjustedDateTime = zonedDateTime
                            .plusDays(handlingCollectorProperties.getNbrDaysAfterHandlingUnitDtToPurge());
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    String purgeDateString = adjustedDateTime.format(formatter);
                    return new SimpleDateFormat(DATE_FORMAT).parse(purgeDateString + " 00:00:00");
                }
            }
        }
        return null;
    }

    private void publishDomainEvent(HandlingUnitDomainEvent domainEvent) throws IOException {
        String pubDomEvent = JsonUtil.asCompactJson(objectMapper, domainEvent);
        domainEventPublisher.publish(pubDomEvent, new MessagePostProcessor() {
            public Message postProcessMessage(Message message) throws JMSException {
                String shipmentUUID = getShipmentId(domainEvent);
                message.setJMSCorrelationID(String.valueOf(domainEvent.getDomainEventHeader().getEventUUID()));
                message.setStringProperty("DOMAIN_EVENT_TYPE", "HANDLINGUNIT");
                message.setStringProperty("MessageType", HandlingUnitDomainEvent.class.getName());
                message.setStringProperty("HANDLINGUNITUUID",
                        String.valueOf(domainEvent.getHandlingUnit().getSystemIdentificationProfile().getUuid()));
                message.setStringProperty("SHIPMENTUUID", shipmentUUID);
                return message;
            }
        });
        log.info("Handling Domain Event Published : {}", pubDomEvent);
        Audit.getInstance().put(AuditKey.OUT, "Handling Domain Event Published:" + pubDomEvent);
    }

    private String getShipmentId(HandlingUnitDomainEvent domainEvent) {
        if (Objects.nonNull(domainEvent.getAssociations())) {
            Association association = domainEvent.getAssociations().stream()
                    .filter(shipment -> shipment.getAssociationLevel().equalsIgnoreCase(SHIPMENT_LEVEL)).findAny()
                    .orElse(null);
            if (association != null && association.getAssociationUUID() != null) {
                return association.getAssociationUUID();
            }
        }
        return null;

    }

    private TransactionStatus getTrxStatus() {
        Audit.getInstance().put(AuditKey.ACTION, AUDIT_KEY);
        DefaultTransactionDefinition txDef = new DefaultTransactionDefinition();
        txDef.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        return transactionManager.getTransaction(txDef);
    }
}
