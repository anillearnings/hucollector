package com.fedex.phuwv.hucollector.util;

public class HandlingUnitCollectorConstant {
	public static final String AUDIT_KEY = "HandlingDomainEventHandler.handle";
	public static final String SHIPMENT_LEVEL = "SHIPMENT";
	public static final String DOMAIN_EVENT_TYPE_KEY = "DOMAIN_EVENT_TYPE";
	public static final String DOMAIN_EVENT_TYPE = "HANDLINGUNIT";
	public static final String MESSAGE_TYPE = "MessageType";
	public static final String HANDLING_UNIT_UUID = "HANDLINGUNITUUID";
	public static final String SHIPMENT_UUID = "SHIPMENTUUID";
	public static final String DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";

}
