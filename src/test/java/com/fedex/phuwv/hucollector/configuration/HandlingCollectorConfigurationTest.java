package com.fedex.phuwv.hucollector.configuration;

import static org.junit.Assert.assertNotEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { HandlingCollectorConfiguration.class }, initializers = ConfigFileApplicationContextInitializer.class)
@ActiveProfiles("L0")
@SpringBootTest
public class HandlingCollectorConfigurationTest {

	@Autowired
	private HandlingCollectorProperties handlingCollectorProperties;

	@Test
	public void givenTestProperties_thenLoadConfigurations() {

		assertNotEquals(null, handlingCollectorProperties);

	}

}
