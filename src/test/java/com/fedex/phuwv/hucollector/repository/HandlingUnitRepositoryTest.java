
package com.fedex.phuwv.hucollector.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.util.CompressionUtil;
import com.fedex.phuwv.util.JsonUtil;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitId;
import com.fedex.sefs.core.handlingunit.v1.api.Association;
import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnit;
import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitDomainEvent;

@ExtendWith(SpringExtension.class)

@ActiveProfiles("L0")

@SpringBootTest
public class HandlingUnitRepositoryTest {

	@Autowired
	private HandlingUnitRepository repository;

	@Autowired
	private ObjectMapper objectMapper;

	private Instant instantTime;

	@BeforeEach
	public void setUp() {
		List<HandlingUnitEntity> dbEntityList = repository
				.findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(
						"521d02f3-9a0b-3ffb-9fa5-2a591e245412");
		repository.deleteInBatch(dbEntityList);
	}

	@AfterEach
	public void cleanUp() {
		List<HandlingUnitEntity> dbEntityList = repository
				.findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(
						"521d02f3-9a0b-3ffb-9fa5-2a591e245412");
		repository.deleteInBatch(dbEntityList); 
	}

	@Test
	public void

			givenHandlingUnitEntity_whenSave_thenPersistHandlingUnitEntity()
					throws JsonParseException, JsonMappingException, IOException {

		HandlingUnitEntity handlingUnitEntity = new HandlingUnitEntity();
		Instant insTime = Instant.now();
		instantTime = Optional.of(insTime.truncatedTo(ChronoUnit.MICROS)).filter(insTime::equals)
				.orElse(insTime.truncatedTo(ChronoUnit.MICROS).plus(1, ChronoUnit.MICROS));

		handlingUnitEntity.setEventType("HANDLINGUNIT_INITIALIZED");

		ZonedDateTime timedZone = ZonedDateTime.now().truncatedTo(ChronoUnit.MICROS);

		handlingUnitEntity.setEventCreateTimestamp(timedZone);
		Date dateNow = Date.from(Instant.now());
		handlingUnitEntity.setRowPurgeDt(dateNow);
		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent1.json", HandlingUnitDomainEvent.class);
		HandlingUnit handlingUnit = handlingUnitDomainEvent.getHandlingUnit();
		HandlingUnitId handlingUnitId = new HandlingUnitId(
				handlingUnitDomainEvent.getHandlingUnit().getSystemIdentificationProfile().getUuid().toString(),
				handlingUnitDomainEvent.getDomainEventHeader().getEventUUID().toString(), instantTime);
		handlingUnitEntity.setHandlingUnitId(handlingUnitId);
		Association association = handlingUnitDomainEvent.getAssociations().stream()
				.filter(shipment -> shipment.getAssociationLevel().equalsIgnoreCase("SHIPMENT")).findAny().get();
		String shipmentUUID = association.getAssociationUUID();
		handlingUnitEntity.setShipmentUUID(shipmentUUID);

		String json = JsonUtil.asCompactJson(objectMapper, handlingUnit);
		byte[] dataImage1 = CompressionUtil.gzip(json);
		handlingUnitEntity.setDataImg1(dataImage1);
		repository.save(handlingUnitEntity);

		List<HandlingUnitEntity> dbEntityList = repository
				.findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(handlingUnitDomainEvent
						.getHandlingUnit().getSystemIdentificationProfile().getUuid().toString());
		HandlingUnitEntity dbEntity = dbEntityList.get(0);
		assertEquals(handlingUnitDomainEvent.getHandlingUnit().getSystemIdentificationProfile().getUuid().toString(),
				dbEntity.getId().getHandlingUnitUUID());
		assertEquals("0fc79564-10d7-35e6-9f4e-25668a165410", dbEntity.getHandlingUnitId().getEventUUID());
		assertEquals("HANDLINGUNIT_INITIALIZED", dbEntity.getEventType());
		assertEquals(timedZone, dbEntity.getEventCreateTimestamp());
		assertEquals(instantTime, dbEntity.getId().getRowUpdateTmstp());
		assertEquals("4841f051-28ce-3e03-98db-f18c112fd823", dbEntity.getShipmentUUID());
		assertEquals(json, CompressionUtil.gunzip(dbEntity.getDataImg1()));
		assertEquals(null, dbEntity.getDataImg2());

	}

	@Test()
	public void givenTwoHandlingUnitEntitiesWithSameCompositeKeys_whenSave_thenThrowDataIntegrityViolationException()
			throws JsonParseException, JsonMappingException, IOException {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);
		HandlingUnit handlingUnit = handlingUnitDomainEvent.getHandlingUnit();
		String json = JsonUtil.asCompactJson(objectMapper, handlingUnit);

		assertThrows(DataIntegrityViolationException.class, () -> {
			for (int i = 0; i < 2; i++) {
				HandlingUnitEntity entity = new HandlingUnitEntity();
				HandlingUnitId handlingUnitID = new HandlingUnitId("521d02f3-9a0b-3ffb-9fa5-2a591e245412",
						"c0e8c19d-4912-3a32-9c2a-d6c26bbf3c86", instantTime);
				entity.setHandlingUnitId(handlingUnitID);

				entity.setEventCreateTimestamp((ZonedDateTime.now()));
				entity.setShipmentUUID("e869a92a-8c14-3090-a830-efa161f82721");

				byte[] dataImage1 = CompressionUtil.gzip(json);
				entity.setDataImg1(dataImage1);
				repository.save(entity);
			}
		});

	}

	@Test
	public void givenHandlingUnitEntityWithOversizeHandlingUnitUUID_onSave_shallThrowException()
			throws JsonParseException, JsonMappingException, IOException {
		HandlingUnitEntity handlingUnitEntity = new HandlingUnitEntity();
		HandlingUnitId handlingUnitId = new HandlingUnitId(
				"521d02f3-9a0b-3ffb-9fa5-2a591e245114-1234567890123456789012345678905646",
				"c0e8c19d-4912-3a32-9c2a-d6c26bbf3c86", instantTime);
		handlingUnitEntity.setHandlingUnitId(handlingUnitId);
		Throwable thrown = catchThrowable(() -> {
			repository.save(handlingUnitEntity);
		});
		assertThat(thrown.getMessage()).isNotNull();

	}

}
