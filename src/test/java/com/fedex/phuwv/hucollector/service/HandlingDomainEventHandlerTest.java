
package com.fedex.phuwv.hucollector.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.exception.ApplicationException;
import com.fedex.phuwv.common.exception.ResourceException;
import com.fedex.phuwv.common.jms.JMSPublisher;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.hucollector.configuration.HandlingCollectorProperties;
import com.fedex.phuwv.hucollector.repository.HandlingUnitRepository;
import com.fedex.phuwv.hucollector.repository.HandlingUnitXRefRepository;
import com.fedex.phuwv.util.BytesUtil;
import com.fedex.phuwv.util.CompressionUtil;
import com.fedex.phuwv.util.JsonUtil;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitEntity;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitId;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefEntity;
import com.fedex.phuwv.view.handlingunitentity.entity.HandlingUnitXRefId;
import com.fedex.sefs.core.handlingunit.v1.api.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.jms.JMSException;
import javax.jms.Message;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.fedex.phuwv.hucollector.util.HandlingUnitCollectorConstant.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.*;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
 class HandlingDomainEventHandlerTest {

    @Autowired
    private JMSPublisher domainEventPublisher;

    @Autowired
    @Qualifier("defaultObjectMapper")
    private ObjectMapper objectMapper;

    @Autowired
    private HandlingUnitRepository handlingRepository;

    @Autowired
    private HandlingUnitXRefRepository handlingUnitXrefRepository;

    @Autowired
    private HandlingDomainEventHandler handlingDomainEventHandler;

    @Autowired
    private HandlingCollectorProperties handlingCollectorProperties;

    @Autowired
    private PlatformTransactionManager transactionManager;

    private static final String handlingUnitUUID = "ec24af9d-5d02-38a0-b996-91f41603ac49";
    
    
    final Logger logger = LoggerFactory.getLogger(HandlingDomainEventHandlerTest.class);

//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        domainEventPublisher = Mockito.mock(JMSPublisher.class);
//        Mockito.spy(domainEventPublisher);
////        List<HandlingUnitEntity> handlingUnitEntities = handlingRepository
////                .findByHandlingUnitId_handlingUnitUUID(handlingUnitUUID);
////        handlingRepository.deleteInBatch(handlingUnitEntities);
////        List<HandlingUnitXRefEntity> handlingUnitXrefEntities = handlingUnitXrefRepository
////                .findByHandlingUnitRefId_handlingUnitUUID(handlingUnitUUID);
////        handlingUnitXrefRepository.deleteInBatch(handlingUnitXrefEntities);
//    }

    

//    @AfterEach
//    public void cleanup() {
//        List<HandlingUnitEntity> handlingUnitEntities = handlingRepository
//                .findByHandlingUnitId_handlingUnitUUID(handlingUnitUUID);
//        handlingRepository.deleteInBatch(handlingUnitEntities);
//        List<HandlingUnitXRefEntity> handlingUnitXrefEntities = handlingUnitXrefRepository
//                .findByHandlingUnitRefId_handlingUnitUUID(handlingUnitUUID);
//        handlingUnitXrefRepository.deleteInBatch(handlingUnitXrefEntities);
//    }

//    @Test
//     void testHandleSuccess() throws Exception {
//        HandlingUnitEntity handlingUnitEntity = new HandlingUnitEntity();
//        Date dateNow = Date.from(Instant.now());
//        Instant currentTime = Instant.now();
//        HandlingUnitId handlingUnitId = new HandlingUnitId("b03333f3-4022-39df-a347-54665f1987d0", "cd49c53e-11ac-362c-bb38-256cb9e52f80", currentTime);
//        handlingUnitEntity.setHandlingUnitId(handlingUnitId);
//        //entity.setEventCreateTmstp(ZonedDateTime.now());
//        handlingUnitEntity.setShipmentUUID("9b05234b-b6a4-3d7d-a919-a58b5661bec2");
//        handlingUnitEntity.setEventCreateTimestamp(ZonedDateTime.now());
//        handlingUnitEntity.setRowPurgeDt(dateNow);
//        handlingUnitEntity.setEventType("HANDLINGUNIT_INITIALIZED");
//        HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
//                "HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);
//        try {
//            Mockito.verify(handlingDomainEventHandler, Mockito.times(1)).handle(new GenericMessage<>(handlingUnitDomainEvent));
//        } catch (Exception e) {
//        	logger.error("error in testHandleSuccess"+e);
//        }
//    }

    @Test
     void givenHandlingUnitDomainEvent_whenSave_HandlingUnitEntityWillBePersisted()
            throws Exception {
//        HandlingUnitEntity entity = new HandlingUnitEntity();
//        Date dateNow = Date.from(Instant.now());
//        Instant currentTime = Instant.now();
//        HandlingUnitId handlingUnitId = new HandlingUnitId("ec24af9d-5d02-38a0-b996-91f41603ac49", "f85139a0-d2ad-11eb-61a5-c3c6c2620f01", currentTime);
//        entity.setHandlingUnitId(handlingUnitId);
//        entity.setShipmentUUID("9b05234b-b6a4-3d7d-a919-a58b5661bec2");
//        entity.setEventCreateTimestamp(ZonedDateTime.now());
//        entity.setRowPurgeDt(dateNow);
//        entity.setEventType("HANDLINGUNIT_INITIALIZED");
        HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
                "HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);
        handlingDomainEventHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
//        HandlingUnitEntity dbEntity = handlingRepository
//                .findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(
//                        "ec24af9d-5d02-38a0-b996-91f41603ac49")
//                .get(0);
//        HandlingUnit handlingUnit = handlingUnitDomainEvent.getHandlingUnit();
//        String json = JsonUtil.asCompactJson(objectMapper, handlingUnit);
//        byte[] dbDataImage1 = CompressionUtil.gzip(json);
//        byte[] dbDataImage2 = dbEntity.getDataImg2();
//        entity.setDataImg1(dbDataImage1);
//        handlingRepository.save(entity);
//        String entityJson = BytesUtil.convertDataImageToJson(dbDataImage1, dbDataImage2);
//        assertThat(entityJson).isEqualTo(json);
//        List<HandlingUnitXRefEntity> huXrefs;
//        huXrefs = handlingUnitXrefRepository
//                .findByHandlingUnitRefId_handlingUnitUUID("ec24af9d-5d02-38a0-b996-91f41603ac49");
//        assertEquals("267251491547", huXrefs.get(0).getHandlingUnitRefId().getTrackingNbr());

    }

//    @Test
//     void testToValidateSaveHandlingUnit()
//            throws Exception {
//        HandlingUnitEntity entity = new HandlingUnitEntity();
//        Date dateNow = Date.from(Instant.now());
//        Instant currentTime = Instant.now();
//        HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
//                "HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);
//        HandlingUnit handlingUnit = handlingUnitDomainEvent.getHandlingUnit();
//
//        HandlingUnitId handlingUnitId = new HandlingUnitId(String.valueOf(handlingUnitDomainEvent.getHandlingUnit().getSystemIdentificationProfile().getUuid()), String.valueOf(handlingUnitDomainEvent.getDomainEventHeader().getEventUUID()), currentTime);
//        entity.setHandlingUnitId(handlingUnitId);
//        String shipmentUUID = null;
//        entity.setShipmentUUID(shipmentUUID);
//        entity.setEventCreateTimestamp(ZonedDateTime.now());
//        entity.setRowPurgeDt(dateNow);
//        entity.setEventType("HANDLINGUNIT_INITIALIZED");
//
//        String json = JsonUtil.asCompactJson(objectMapper, handlingUnitDomainEvent);
//        convertJsonToRaw(json, entity);
//        handlingRepository.save(entity);
//        HandlingUnit huId = handlingUnitDomainEvent.getHandlingUnit();
//        HandlingUnitIdentificationProfile huidp = huId.getIdentificationProfile();
//        SystemIdentificationProfile idProfile = handlingUnit.getSystemIdentificationProfile();
//        TransactionStatus transactionStatus = getTrxStatus();
//        List<com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitId> huids = huidp.getHandlingUnitIds();
//        for (com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitId hu : huids) {
//            HandlingUnitXRefId key = new HandlingUnitXRefId();
//            HandlingUnitXRefEntity crossRefEntity = new HandlingUnitXRefEntity();
//            String trackingNbr = hu.getId();
//            String trackNbrType = hu.getIdType();
//            key.setHandlingUnitUUID(String.valueOf(idProfile.getUuid()));
//            key.setTrackingNbrType(trackNbrType);
//            key.setTrackingNbr(trackingNbr);
//            crossRefEntity.setHandlingUnitRefId(key);
//            crossRefEntity.setRowPurgeData(getDefaultPurgeDates());
//            try {
//                handlingUnitXrefRepository.save(crossRefEntity);;
//                transactionManager.commit(transactionStatus);
//            }
//            catch (Exception e){
//            	logger.error("error in testToValidateSaveHandlingUnit"+e);
//            }
//
//        }
//        assertNotNull(json);
//    }
//        private void convertJsonToRaw (String compactJson, HandlingUnitEntity entity) throws IOException {
//            int maxColumnLength = handlingCollectorProperties.getMaxDataImgColumnLength();
//            int totalColumnLength = handlingCollectorProperties.getMaxDataImgColumn() * maxColumnLength;
//            byte[] byteBuff = CompressionUtil.gzip(compactJson);
//            List<byte[]> dataImages = BytesUtil.convertJsonByteArray(byteBuff, maxColumnLength, totalColumnLength);
//            int dataImageToSet = dataImages.size();
//            entity.setDataImg1(dataImages.get(0));
//            if (dataImageToSet > 1) {
//                entity.setDataImg2(dataImages.get(1));
//            }
//        }
//  
//        @Test
//         void givenHandlingUnitDomainEventWithNoDomainEventHeader_whenHandle_WillThrowApplicationException ()
//            throws Exception {
//            HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
//                    "HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);
//            handlingUnitDomainEvent.setDomainEventHeader(null);
//            Throwable thrown = catchThrowable(() -> handlingDomainEventHandler.handle(new GenericMessage<HandlingUnitDomainEvent>(handlingUnitDomainEvent)));
//            assertThat(thrown).isInstanceOf(ApplicationException.class);
//        }
//
//         @Test
//          void givenHandlingUnitDomainEventPublishedToJMS_whenHandle_HandlingUnitEntityWillBePersisted ()
//             throws Exception {
//             HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
//                     "HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);
//             try {
//
//                 handlingDomainEventHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
//                 publishDomainEvent(handlingUnitDomainEvent);
//             } catch (Exception e) {
//                 logger.error("error in givenHandlingUnitDomainEventPublishedToJMS_whenHandle_HandlingUnitEntityWillBePersisted "+e);
//             }
//             Thread.sleep(5000);
//             List<HandlingUnitEntity> dbEntity = handlingRepository
//                     .findByHandlingUnitId_handlingUnitUUID("ec24af9d-5d02-38a0-b996-91f41603ac49");
//             assertEquals("ec24af9d-5d02-38a0-b996-91f41603ac49", dbEntity.get(0).getHandlingUnitId().getHandlingUnitUUID());
//             assertEquals("f85139a0-d2ad-11eb-61a5-c3c6c2620f01", dbEntity.get(0).getHandlingUnitId().getEventUUID());
//             assertNotEquals(null, dbEntity.get(0).getRowPurgeDt());
//
//             Date calcPurgeDate = handlingDomainEventHandler.getCalculatePurgeDateByHandling(handlingUnitDomainEvent);
//
//             ZonedDateTime releaseDateTime = handlingUnitDomainEvent.getHandlingUnit().getIdentificationProfile()
//                     .getIntendedShipDate();
//
//             assertEquals(ZonedDateTime.parse("2021-06-18T00:00Z"), releaseDateTime);
//             ZonedDateTime adjustDateTime = releaseDateTime
//                     .plusDays(handlingCollectorProperties.getNbrDaysAfterHandlingUnitDtToPurge());
//             DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//             String purgeDateStr = adjustDateTime.format(formatter);
//
//             Date calculatedDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(purgeDateStr + " 00:00:00");
//             assertEquals(calculatedDate, calcPurgeDate);
//         }
//
//        @Test
//        void givenDuplicateHandlingUnitDomainEventPublishedToJMS_whenHandle_TwoHandlingUnitEntityWillBePersisted
//        ()
//            throws ResourceException, Exception {
//            HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
//                    "HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);
//            for (int i = 0; i < 2; i++) {
//            	handlingDomainEventHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
//                publishDomainEvent(handlingUnitDomainEvent);
//            }
//            Thread.sleep(5000);
//            List<HandlingUnitEntity> handlingUnitEntities = handlingRepository
//                    .findByHandlingUnitId_handlingUnitUUID("ec24af9d-5d02-38a0-b996-91f41603ac49");
//            assertNotNull(handlingUnitEntities);
//
//            for (HandlingUnitEntity handlingUnitEntity : handlingUnitEntities) {
//
//                assertEquals("ec24af9d-5d02-38a0-b996-91f41603ac49",
//                        handlingUnitEntity.getHandlingUnitId().getHandlingUnitUUID());
//                assertEquals("f85139a0-d2ad-11eb-61a5-c3c6c2620f01", handlingUnitEntity.getHandlingUnitId().getEventUUID());
//                assertEquals(HandlingUnitEventType.HANDLINGUNIT_INITIALIZED.name(), handlingUnitEntity.getEventType());
//                assertNotEquals(null, handlingUnitEntity.getRowPurgeDt());
//            }
//        }

      

        private void publishDomainEvent (HandlingUnitDomainEvent domainEvent) throws IOException {
            String domainEventJson = JsonUtil.asCompactJson(objectMapper, domainEvent);
            domainEventPublisher.publish((domainEventJson), new MessagePostProcessor() {
                public Message postProcessMessage(Message message) throws JMSException {
                    message.setJMSCorrelationID(String.valueOf(domainEvent.getDomainEventHeader().getEventUUID()));
                    message.setStringProperty("Source", "HANDLING_UNIT");
                    message.setStringProperty("MessageType", HandlingUnitDomainEvent.class.getName());
                    return message;
                }
            });

        }
    private Date getDefaultPurgeDates() {
        Instant instant = Instant.now();
        Instant purgeInstant = instant.plus(handlingCollectorProperties.getMaxRetentionDays(), ChronoUnit.DAYS);
        Date dates = Date.from(purgeInstant);
        Calendar calendars = Calendar.getInstance();
        calendars.setTime(dates);
        calendars.set(Calendar.HOUR_OF_DAY, 0);
        calendars.set(Calendar.MINUTE, 0);
        calendars.set(Calendar.SECOND, 0);
        calendars.set(Calendar.MILLISECOND, 0);
        return calendars.getTime();
    }
    private TransactionStatus getTrxStatus() {
        Audit.getInstance().put(AuditKey.ACTION, AUDIT_KEY);
        DefaultTransactionDefinition txDef = new DefaultTransactionDefinition();
        txDef.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        return transactionManager.getTransaction(txDef);
    }
}
