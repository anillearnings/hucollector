package com.fedex.phuwv.hucollector.util;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
 
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class HandlingUnitServiceConstantsTest {
	
 @InjectMocks
 private HandlingUnitCollectorConstant constants;
  
    @Test
    public void testConstants() {
    	
    	assertThat(HandlingUnitCollectorConstant.AUDIT_KEY).isEqualTo("HandlingDomainEventHandler.handle");
    	assertThat(HandlingUnitCollectorConstant.DATE_FORMAT).isEqualTo("yyyy-MM-dd hh:mm:ss");
    	assertThat(HandlingUnitCollectorConstant.DOMAIN_EVENT_TYPE).isEqualTo("HANDLINGUNIT");
    	assertThat(HandlingUnitCollectorConstant.HANDLING_UNIT_UUID).isEqualTo("HANDLINGUNITUUID");
    	assertThat(HandlingUnitCollectorConstant.MESSAGE_TYPE).isEqualTo("MessageType");
    	assertThat(HandlingUnitCollectorConstant.SHIPMENT_LEVEL).isEqualTo("SHIPMENT");
    	assertThat(HandlingUnitCollectorConstant.SHIPMENT_UUID).isEqualTo("SHIPMENTUUID");
    }
    
}